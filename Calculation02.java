//Title:  Take home assignment for Developer in QA Automation role
//Author: Tomas Kozlik
//Date:   15.9.2020
//Rev 01 17.9.2020

//Calculation class

package com.company;

import java.util.Arrays;
import java.util.Collections;

public class Calculation02 {

    private int sum = 0;

    public int arraySum(Integer[] arrayUserInput) {
        Arrays.sort(arrayUserInput, Collections.reverseOrder());
        return sum = arrayUserInput[0] + arrayUserInput[1] + arrayUserInput[2];
    }

    public String toString() {
        return "Sum of 3 greatest numbers in an array: " + sum;
    }
}