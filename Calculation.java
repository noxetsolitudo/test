//Title:  Take home assignment for Developer in QA Automation role
//Author: Tomas Kozlik
//Date:   15.9.2020

//Calculation class

package com.company;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Calculation {

    private Random random = new Random();//decided to use a Random method

    //and assumed array with 5 variables
    private int rnd1 = random.nextInt(100);//looking at random numbers between 0-99
    private int rnd2 = random.nextInt(100);
    private int rnd3 = random.nextInt(100);
    private int rnd4 = random.nextInt(100);
    private int rnd5 = random.nextInt(100);

    private int sum = 0;

    private Integer[] array = {rnd1, rnd2, rnd3, rnd4, rnd5};//array filled with random numbers

    public int arraySum() {//sum of 3 biggest numbers in an array
        Arrays.sort(array, Collections.reverseOrder());//reversing order from greatest to smallest
        return sum = array[0] + array[1] + array[2];//sum of first three numbers, in this case three greatest
    }

    public String toString() {
        return "Array in reverse order print out: " + Arrays.toString(array) + "\nSum of 3 greatest numbers in an array: " + sum;//print out of above for visual check and final sum of three greatest numbers in an array
    }
}
