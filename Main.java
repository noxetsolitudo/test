//Title:  Take home assignment for Developer in QA Automation role
//Author: Tomas Kozlik
//Date:   15.9.2020
//Rev 01 17.9.2020

//Driver class
package com.company;

public class Main {

    public static void main(String[] args) {

        Integer[] array = {10, 5, 0, 15, 1};

        Calculation02 calculation02 = new Calculation02();
        calculation02.arraySum(array);

        System.out.println(calculation02);
    }
}