//by Robo

package com.company;

public class Calculation03 {

    public static void main(String[] args) {
        int[] input = {10, 5, 0, 15, 1};
        int output = sum3GreatestNumbersInArray(input);
        System.out.println(output);
    }

    private static int sum3GreatestNumbersInArray(int[] input) {
        int first = 0;
        int second = 0;
        int third = 0;

        for (int i = 0; i < input.length; i++) {
            if (input[i] > first) {
                third = second;
                second = first;
                first = input[i];
            } else if (input[i] > second) {
                third = second;
                second = input[i];
            } else if (input[i] > third) {
                third = input[i];
            }
        }

        return first + second + third;
    }
}
