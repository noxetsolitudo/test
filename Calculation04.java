//by Robo

package com.company;

public class Calculation04 {

    public static void main(String[] args) {
        int[] input = {10, 5, 0, 15, 1};
        int output = sum3GreatestNumbersInArray(input);
        System.out.println(output);
    }

    private static int sum3GreatestNumbersInArray(int[] input) {
        int first = 0;
        int second = 0;
        int third = 0;

        for (int value : input) {
            if (value > first) {
                third = second;
                second = first;
                first = value;
            } else if (value > second) {
                third = second;
                second = value;
            } else if (value > third) {
                third = value;
            }
        }

        return first + second + third;
    }
}
