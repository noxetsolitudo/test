package com.company;

import java.util.Arrays;
import java.util.Comparator;

public class Calculation05 {

    public static void main(String[] args) {
        int[] input = {10, 5, 0, 15, 1};
        int output = sum3GreatestNumbersInArray(input);
        System.out.println(output);
    }

    private static int sum3GreatestNumbersInArray(int[] input) {
        return Arrays.stream(input)
                .boxed()
                .sorted(Comparator.reverseOrder())
                .limit(3)
                .mapToInt(Integer::intValue)
                .sum();
    }
}
